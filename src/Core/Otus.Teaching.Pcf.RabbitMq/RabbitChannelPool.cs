﻿using RabbitMQ.Client;
using System.Diagnostics;
using System.Threading;

namespace Otus.Teaching.Pcf.RabbitMq
{
    internal sealed class RabbitChannelPool
    {
        [DebuggerDisplay("{Channel}")]
        private struct ChannelWrapper
        {
            public IModel Channel;
        }

        private IModel _currentChannel;
        private readonly ChannelWrapper[] _channels;
        
        public RabbitChannelPool(int maximumRetained) => _channels = new ChannelWrapper[maximumRetained - 1];

        public IModel Get(IConnection connection)
        {
            var item = _currentChannel;
            if (item != null && Interlocked.CompareExchange(ref _currentChannel, null, item) == item)
                return item;

            for (var i = 0; i < _channels.Length; i++)
            {
                item = _channels[i].Channel;
                if (item != null && Interlocked.CompareExchange(ref _channels[i].Channel, null, item) == item)
                {
                    return item;
                }
            }

            return connection.CreateModel();
        }
        
        public void Return(IModel channel)
        {
            if (channel == null)
                return;

            if (!channel.IsOpen)
            {
                channel.Dispose();
                return;
            }

            if (_currentChannel == null && Interlocked.CompareExchange(ref _currentChannel, channel, null) == null)
                return;

            for (var i = 0; i < _channels.Length; i++)
            {
                if (Interlocked.CompareExchange(ref _channels[i].Channel, channel, null) == null)
                {
                    return;
                }
            }
            channel.Dispose();
        }
    }
}
