﻿namespace Otus.Teaching.Pcf.RabbitMq.Options
{
    public class QueueSetting
    {
        public string Queue { get; set; }
        public string Exchange { get; set; } = string.Empty;
        public string ExchangeType { get; set; }
        public string[] BindingKeys { get; set; }
        public ushort? PrefetchCount { get; set; }
    }
}
