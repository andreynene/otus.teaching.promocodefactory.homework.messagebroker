﻿namespace Otus.Teaching.Pcf.RabbitMq.Options
{
    public class RabbitSetting
    {
        public string UserName { get; set; } = "guest";
        public string Password { get; set; } = "guest";
        public string HostName { get; set; } = "localhost";
        public string VirtualHost { get; set; } = "/";
        public int? Port { get; set; } = -1;
        public int ChannelPoolSize { get; set; } = 20;
        public uint? MaxMessageSize { get; set; }
        public ushort? RequestedChannelMax { get; set; } = 2047;
    }
}
