﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Otus.Teaching.Pcf.RabbitMq.Managers;
using Otus.Teaching.Pcf.RabbitMq.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.RabbitMq.HostedServices
{
    public abstract class QueueListener : BackgroundService
    {
        private readonly IQueueReceiver _queueReceiver;
        private readonly QueueListenerSetting _queueListenerSetting;

        public QueueListener(IQueueReceiver queueReceiver, IOptions<QueueListenerSetting> options)
        {
            _queueReceiver = queueReceiver;
            _queueListenerSetting = options.Value;
        }

        protected abstract Task HandleMessageAsync(string message);

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (!_queueListenerSetting.Enabled)
                return;

            await Task.Delay(TimeSpan.FromSeconds(15), stoppingToken).ConfigureAwait(false);

            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {

                    _queueReceiver.Receive(HandleMessageAsync);
                }
                catch (Exception ex)
                {
                    // ignored
                }

                await Task.Delay(_queueListenerSetting.Period, stoppingToken);
            }
        }
    }
}