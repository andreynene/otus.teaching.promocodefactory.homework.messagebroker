﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using System;
using System.Text;
using RabbitMQ.Client.Events;
using Otus.Teaching.Pcf.RabbitMq.Options;
using System.Collections.Concurrent;

namespace Otus.Teaching.Pcf.RabbitMq.Managers
{
    public interface IRabbitManager
    {
        void Publish(string message, QueueSetting queueSetting, string routingKey = null, string correlationId = null);
        void Consume(QueueSetting queueSetting, Action<string, string> handleMessage);
    }

    internal class RabbitManager : IRabbitManager
    {
        private static readonly object _lockObject = new object();
        private IConnection connection;
        private RabbitChannelPool _channelPool;
        private readonly RabbitSetting _rabbitSetting;
        private readonly ConcurrentDictionary<string, string> _rabbitQueue = new ConcurrentDictionary<string, string>();

        public RabbitManager(IOptions<RabbitSetting> rabbitOptions)
        {
            _rabbitSetting = rabbitOptions.Value;
            _channelPool = new RabbitChannelPool(_rabbitSetting.ChannelPoolSize);
        }

        public void Publish(string message, QueueSetting queueSetting, string routingKey = null, string correlationId = null)
        {
            if (message == null) return;

            try
            {
                var sendBytes = Encoding.UTF8.GetBytes(message);
                var connection = GetConnection();
                var channel = _channelPool.Get(connection);

                try
                {
                    var queue = EnsureProducerQueue(channel, queueSetting);
                    var properties = channel.CreateBasicProperties();

                    properties.Persistent = true;
                    properties.CorrelationId = correlationId ?? Guid.NewGuid().ToString();
                    channel.BasicPublish(exchange: queueSetting.Exchange, routingKey ?? queue, properties, sendBytes);
                }
                catch (Exception ex)
                {
                    DeleteQueue(queueSetting);
                    throw;
                }
                finally
                {
                    _channelPool.Return(channel);
                }
            }
            catch (Exception ex)
            {
                // ignored
            }
        }


        public void Consume(QueueSetting queueSetting, Action<string, string> handleMessage)
        {
            var channel = GetConnection().CreateModel();
            var consumer = new EventingBasicConsumer(channel);

            var queue = EnsureConsumerQueue(channel, queueSetting);

            consumer.Received += (ch, ea) =>
            {
                string content = Encoding.UTF8.GetString(ea.Body.ToArray());
                handleMessage(content, ea.BasicProperties.CorrelationId);
                channel.BasicAck(ea.DeliveryTag, false);
            };
            _ = channel.BasicConsume(queue, false, consumer);
        }

        private IConnection GetConnection()
        {
            lock (_lockObject)
            {
                connection ??= RabbitHelper.GetRabbitConnection(_rabbitSetting);
                return connection;
            }
        }

        private string EnsureProducerQueue(IModel channel, QueueSetting queueSetting)
        {
            lock (_lockObject)
            {
                if (string.IsNullOrEmpty(queueSetting.Exchange))
                    return EnsureQueue(channel, queueSetting);

                return EnsureProducerExchange(channel, queueSetting);
            }
        }

        private string EnsureProducerExchange(IModel channel, QueueSetting queueSetting)
        {
            EnsureExchange(channel, queueSetting);

            if (string.IsNullOrEmpty(queueSetting.Queue) && (queueSetting.BindingKeys?.Length ?? 0) == 0)
                return null;

            var queueName = EnsureQueue(channel, new QueueSetting { Queue = queueSetting.Queue });

            EnsureQueueBind(channel, queueSetting.Exchange, queueName, queueSetting.BindingKeys);

            return queueName;
        }

        private string EnsureConsumerQueue(IModel channel, QueueSetting queueSetting)
        {
            lock (_lockObject)
            {
                if (string.IsNullOrEmpty(queueSetting.Exchange))
                    return EnsureQueue(channel, queueSetting, true);

                return EnsureConsumerExchange(channel, queueSetting);
            }
        }

        private string EnsureConsumerExchange(IModel channel, QueueSetting queueSetting)
        {
            EnsureExchange(channel, queueSetting);
            var queueName = EnsureQueue(channel, new QueueSetting { Queue = queueSetting.Queue, PrefetchCount = queueSetting.PrefetchCount }, true);
            EnsureQueueBind(channel, queueSetting.Exchange, queueName, queueSetting.BindingKeys);
            return queueName;
        }

        private string EnsureQueue(IModel channel, QueueSetting queueSetting, bool needSetBasicQos = false)
        {
            var queueName = queueSetting.Queue ?? "";

            if (!_rabbitQueue.ContainsKey(queueName))
            {
                var result = channel.QueueDeclare(queueName, true, false, false, null);
                if (needSetBasicQos && queueSetting.PrefetchCount > 0)
                    channel.BasicQos(prefetchSize: 0, prefetchCount: queueSetting.PrefetchCount.Value, global: false);

                _rabbitQueue.TryAdd(queueName, result.QueueName);
            }

            return _rabbitQueue[queueName];
        }

        private void EnsureExchange(IModel channel, QueueSetting queueSetting)
        {
            var exchangeQueue = $"{queueSetting.Exchange}-{queueSetting.ExchangeType}";

            if (!_rabbitQueue.ContainsKey(exchangeQueue))
            {
                channel.ExchangeDeclare(exchange: queueSetting.Exchange, type: queueSetting.ExchangeType);
                _rabbitQueue.TryAdd(exchangeQueue, null);
            }
        }

        private void EnsureQueueBind(IModel channel, string exchange, string queueName, string[] bindingKeys)
        {
           
            if (bindingKeys?.Length > 0)
            {
                foreach (var key in bindingKeys)
                {
                    channel.QueueBind( queueName, exchange, key, null);
                }
            }
            else
            {
                channel.QueueBind(queueName, exchange, "", null);
            }
        }

        private void DeleteQueue(QueueSetting queueSetting)
        {
            if (!string.IsNullOrEmpty(queueSetting.Queue))
            {
                if (_rabbitQueue.ContainsKey(queueSetting.Queue))
                {
                    _rabbitQueue.TryRemove(queueSetting.Queue, out _);
                }
                return;
            }

            var exchangeQueue = $"{queueSetting.Exchange}-{queueSetting.ExchangeType}";
            if (!_rabbitQueue.ContainsKey(exchangeQueue))
            {
                _rabbitQueue.TryRemove(exchangeQueue, out _);
            }
        }
    }
}