﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Integration.Dto
{
    public class PartnerNotifyDto
    {
        public Guid PartnerId { get; set; }

        public string Notify { get; set;}
    }
}
